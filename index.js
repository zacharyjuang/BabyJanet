/*	(C) Daniel Hakimi 2018
*
*   /u/_BabyJanet is a reddit bot that responds to *any* user requests.
*
*   This program is free software: you can redistribute it and/or modify
*   it under the terms of the GNU Affero General Public License as published by
*   the Free Software Foundation, either version 3 of the License, or
*   (at your option) any later version.
*
*	Built with help from the following article: https://blog.syntonic.io/2017/07/07/reddit-bot-nodejs-example/.
*   This program is an OpenWhisk action.
*
*   @params are the parameters kept in openwhisk. Here, that's api/login info.
*   @return some json object, generally a blank one because who cares.
*       The outer-level function call is made in the return statement, not
*       because it returns anything important, but because this is how you get
*       OpenWhisk to wait for your promise to resolve.
*/

function processComment(params) {

    const snoowrap = require("snoowrap");
    cactusList = require("./cacti/cactusList.json")

    const r = new snoowrap({ //Get your own reddit credentials
        userAgent: "node:BabyJanet:1.0.0 (by /u/DanHakimi)",
        clientId: params.CLIENT_ID,
        clientSecret: params.CLIENT_SECRET,
        username: params.REDDIT_USER,
        password: params.REDDIT_PASS
    });

    return r.getInbox( {"filter":"mentions", "append":false } )
        .then( newMentions, console.error );

    // Call this for each mention in your inbox
    function newMentions( mentions ) {
    	if (!mentions) {return {};} // do nothing, fast. Don't waste the time.
    		// Yes, ![list] is valid javascriptanese
    	mentions.reduce( processInbox, Promise.resolve(false) );

    	function processInbox ( status, nextMessage )
    	{	/*	Status is an accumulator that promises false until we've got a read
    				message, then true once we have it. */
    		return status.then( checkMessage );

    		function checkMessage( foundSaved ) // when status gives true,
    		{
    			if (foundSaved) { return true; }
    				// the last message was read, so we're done.

    			r.getComment(nextMessage.id).fetch().then(handleComment, fetchError);

    			function handleComment( c )
    			{
    				if (c.saved) { return true; }

    				handleMention(c);
    				return c.save().then(f);
    				function f() { return false; }
    			}

                function fetchError( e )
                {
                    console.error("Error fetching comment " + nextMessage.id);
                }
    		}
    	}

        return {};
    }

    // Call this for each new mention, passing a mention in comment form
    function handleMention(comment) {
        /*	Replies to comment with the URL to a random cactus from
            /cacti/cactusList.json, which is just formatted as an array (not json)
            Return a success value */

        cactusID = Math.floor(Math.random() * cactusList.length);
        cactusURL = cactusList[cactusID];
        c = "[Here you go!](" + cactusURL + ")";

        console.log("Sending reply to comment " + comment.id);

        return comment.reply(c).then( t, rateLimit );
        function t() { return true; }
        function rateLimit() {
            console.error("Hit a Limit!");
            return false;
        }
    }

}

exports.main = processComment;
